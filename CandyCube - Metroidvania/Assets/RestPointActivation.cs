using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RestPointActivation : MonoBehaviour
{

    public GameObject bard;

    public GameObject fire;

    public GameObject RespawnPoint;

    public GameObject CampfireWwiseObject;

    public AK.Wwise.Event campfireAmbience;
    public AK.Wwise.Event campfireLight;
    public AK.Wwise.Event loreTeleport;

    public void TurnOff()
    {
        this.gameObject.SetActive(false);
    }

    public void DeactivateRestSpot()
    {
        fire.SetActive(false);
        bard.SetActive(false);
        campfireAmbience.Stop(CampfireWwiseObject);

        this.gameObject.SetActive(true);
    }

    public void ActivateRestSpot()
    {
        if(DestructionDodger.instance.currentRestPoint != null)
        {
            DestructionDodger.instance.currentRestPoint.DeactivateRestSpot();
        }
            
        DestructionDodger.instance.currentRestPoint = this;
        DestructionDodger.instance.respawnPosition = RespawnPoint;

        fire.SetActive(true);

        bard.SetActive(true);

        campfireAmbience.Post(CampfireWwiseObject);
        loreTeleport.Post(CampfireWwiseObject);
        campfireLight.Post(CampfireWwiseObject);
    }

}
