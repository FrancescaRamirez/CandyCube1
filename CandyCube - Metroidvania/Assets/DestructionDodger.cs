using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestructionDodger : MonoBehaviour
{

    public static DestructionDodger instance;

    public RestPointActivation currentRestPoint;
    public GameObject respawnPosition;

    public int transferNumber = 1;

    public bool currentlyTransfering = false;

    // Start is called before the first frame update
    void Awake()
    {
        if (DestructionDodger.instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            DestructionDodger.instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
    }

}
