using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SettingsFunctionality : MonoBehaviour
{
    public InteractiveMusic musicObject;

    public GameObject resumeButton;

    public void Restart()
    {
        StartCoroutine("sceneTransfer");
    }

    public void Quit()
    {
        Debug.Log("Quit!");
        Application.Quit();
    }

    public void PlayGame()
    {
        Player.instance.timeManager.ResumeGame();
        gameObject.SetActive(false);
        Player.instance.playerInput.closeJournal.Post(Player.instance.PlayerAudioManager);
    }

    public void DestroyEverything()
    {
        Destroy(Player.instance.pp.gameObject); //Destroy player
        Player.instance = null; //Clear instance

        Destroy(DestructionDodger.instance.gameObject); //Destroy do not destroy parent
        DestructionDodger.instance = null; //Clear instance
    }

    IEnumerator sceneTransfer()
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(0);

        Player.instance.teleportScreen.SetActive(false);
        Player.instance.timeManager.PauseGame();

        DestroyEverything();

        musicObject.StopMusic();

        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }

}
