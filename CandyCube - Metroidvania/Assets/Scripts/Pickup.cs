using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup : MonoBehaviour
{
    [Tooltip("tick the correct bool for which pickup this is")]
    [SerializeField]
    bool bone;

    [SerializeField]
    bool doubleJump;

    [SerializeField]
    bool wallJump;

    [SerializeField]
    bool dash;

    [SerializeField]
    bool boneArmor;

    [SerializeField]
    bool barkStun;

    public AK.Wwise.Event pickupNoise;

    public GameObject pickupParticle;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {

            if (pickupParticle != null)
            {
                Instantiate(pickupParticle, transform.position, Quaternion.identity);
            }

            if (bone && Player.instance.bam.boneCount < Player.instance.bam.maxBoneCount)
            {
                int initialBoneCount = Player.instance.bam.boneCount;
                Player.instance.bam.boneCount++;
                if(initialBoneCount == Player.instance.bam.maxBoneCount - 1 || Player.instance.bam.boneCount < Player.instance.bam.maxBoneCount)
                {
                    pickupNoise.Post(Player.instance.PlayerAudioManager);
                    Destroy(this.gameObject);
                }
            }
            else if (doubleJump)
            {
                Player.instance.doubleJumpUnlocked = true;
            }
            else if (dash)
            {
                Player.instance.dashUnlocked = true;
            }
            else if (wallJump)
            {
                Player.instance.wallJumpUnlocked = true;
            }
            else if (boneArmor)
            {
                Player.instance.boneArmorUnlocked = true;
            }
            else if (barkStun)
            {
                Player.instance.barkUnlocked = true;
            }
            
            if(!bone)
            {
                pickupNoise.Post(Player.instance.PlayerAudioManager);
                Destroy(this.gameObject);
            }
            
        }
    }

}
