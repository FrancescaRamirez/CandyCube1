using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BehaviorDesigner.Runtime.Tasks
{
    [TaskDescription("This checks to see if there is a wall or void coming up within the distance provided and turns the entity.")]
    public class TurnCheck : Conditional
    {
        [Tooltip("Distance from a wall it will turn")]
        public SharedFloat wallDistance;

        [Tooltip("Does it turn at voids")]
        public SharedBool turnAtVoids;

        [Tooltip("Distance from a void it will turn")]
        public SharedFloat voidDistance;

        [Tooltip("Does it turn automatically")]
        public SharedBool turnAutomatically;

        [Tooltip("Distance from a void it will turn")]
        public SharedGameObject centerRaycastGameObject;

        public SharedBool facingRight;

        Vector2 centerRaycastPosition;
        BoxCollider2D bc;

        public override TaskStatus OnUpdate()
        {
            //Check for walls
            bc = centerRaycastGameObject.Value.GetComponent<BoxCollider2D>();
            centerRaycastPosition = centerRaycastGameObject.Value.transform.position;

            if(centerRaycastGameObject.Value.transform.localScale.x == 1)
            {
                facingRight.Value = false;
            }
            else
            {
                facingRight.Value = true;
            }

            //If there is an obstacle within distance
            if(Physics2D.BoxCast(centerRaycastPosition, new Vector2(bc.size.x, bc.size.y * 0.9f), 0, facingRight.Value ? Vector2.right : Vector2.left , wallDistance.Value, LayerMask.GetMask("Obstacles")))
            {
                if (turnAutomatically.Value)
                {
                    centerRaycastGameObject.Value.transform.localScale = new Vector2(facingRight.Value ? 1 : -1, 1);
                }
                return TaskStatus.Success;
            }

            if (turnAtVoids.Value)
            {
                //Check for voids
                if (Physics2D.BoxCast(new Vector2(facingRight.Value ? centerRaycastPosition.x + (bc.size.x / 2) : centerRaycastPosition.x - (bc.size.x / 2), centerRaycastPosition.y + (bc.size.y * 0.1f)), new Vector2(bc.size.x * 0.9f, bc.size.y), 0, Vector2.down, voidDistance.Value, LayerMask.GetMask("Obstacles")))
                {
                }
                else
                {
                    if (turnAutomatically.Value)
                    {
                        centerRaycastGameObject.Value.transform.localScale = new Vector2(facingRight.Value ? 1 : -1, 1);
                    }
                    return TaskStatus.Success;
                }
            }

            return TaskStatus.Failure;
        }

        public override void OnDrawGizmos()
        {
            Gizmos.color = Color.white;
            //Horizontal cube start
            //Gizmos.DrawWireCube(centerRaycastGameObject.Value.transform.position, new Vector2(bc.size.x, bc.size.y * 0.9f));
            //Vertical cube start
            //Gizmos.DrawWireCube(new Vector2(facingRight.Value ? centerRaycastPosition.x + (bc.size.x / 2) : centerRaycastPosition.x - (bc.size.x / 2), centerRaycastPosition.y + (bc.size.y * 0.1f)), new Vector2(bc.size.x * 0.9f, bc.size.y));
            Gizmos.color = Color.green;
            //Horizontal cube end
            //Gizmos.DrawWireCube(new Vector2(facingRight.Value ? centerRaycastPosition.x + wallDistance.Value : centerRaycastPosition.x - wallDistance.Value, centerRaycastPosition.y), new Vector2(bc.size.x, bc.size.y * 0.9f));
            //Vert cube end
            //Gizmos.DrawWireCube(new Vector2(facingRight.Value ? centerRaycastPosition.x + (bc.size.x / 2) : centerRaycastPosition.x - (bc.size.x / 2), centerRaycastPosition.y + (bc.size.y * 0.1f) - voidDistance.Value), new Vector2(bc.size.x * 0.9f, bc.size.y));
        }

    }

}
