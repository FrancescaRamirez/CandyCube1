using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AnyPortrait;

public class CharacterCostumes : MonoBehaviour
{
    public Transform characterGroup;
    public apPortrait mainCharacter;

    public apPortrait costume1;
    public apPortrait costume2;
    // Start is called before the first frame update
    void Start()
    {
        AttachCostume1();
        AttachCostume2();
    }

    // Update is called once per frame
    void Update()
    {

        ////If you press the number key 1, all costumes will be detached, and costume 1 will be attached.
        //if (Input.GetKeyDown(KeyCode.Alpha1))
        //{
        //    AttachCostume1();
        //}

        ////If you press the number key 2, all costumes will be detached, and costume 2 will be attached.
        //if (Input.GetKeyDown(KeyCode.Alpha2))
        //{
        //    AttachCostume2();
        //}

        ////If you press the number key 3, all full costumes will be detached.
        //if (Input.GetKeyDown(KeyCode.Alpha3))
        //{
        //    DetachCostume1();
        //    DetachCostume2();
        //}

        ////to hide specific mesh images
        //if (Input.GetKeyDown(KeyCode.Alpha4))
        //{
        //    costume1.HideMesh("ScarfBody");
        //}

        ////to show mesh specific images
        //if (Input.GetKeyDown(KeyCode.Alpha5))
        //{
        //    costume1.ShowMesh("ScarfBody");
        //}
    }

    //Register Costume 1 as a child of the Character Group, and synchronize it with the bone movement of the main character.
    private void AttachCostume1()
    {
        //Register as a child of Character Group and initialize Local location.
        //costume1.transform.parent = characterGroup;
        costume1.transform.localPosition = Vector3.zero;

        //Synchronize bone movements.
        costume1.Synchronize(mainCharacter, false, false, false, true, SYNC_BONE_OPTION.MatchFromRoot);

        //Modify the sorting order of the Costume 1 meshes based on the meshes of the Main Character.
        //Scarf
        costume1.SetSortingOrder("ScarfBody", mainCharacter.GetSortingOrder("Head") + 1);
        costume1.SetSortingOrder("RTie", mainCharacter.GetSortingOrder("Torso") - 1);
        costume1.SetSortingOrder("LTie", costume1.GetSortingOrder("RTie") - 1);
        //Kneepads
        costume1.SetSortingOrder("RKneePad", mainCharacter.GetSortingOrder("RFoot") + 1);
        costume1.SetSortingOrder("RLegStrapDown", mainCharacter.GetSortingOrder("RCalf") + 1);
        costume1.SetSortingOrder("RLegStrapUp", mainCharacter.GetSortingOrder("RCalf") + 1);
        costume1.SetSortingOrder("LKneePad", mainCharacter.GetSortingOrder("LFoot") + 1);
        costume1.SetSortingOrder("LLegStrapDown", mainCharacter.GetSortingOrder("LCalf") + 1);
        costume1.SetSortingOrder("LLegStrapUp", mainCharacter.GetSortingOrder("LCalf") + 1);
    }

    //Register Costume 2 as a child of the Character Group, and synchronize it with the bone movement of the main character.
    private void AttachCostume2()
    {
        //Register as a child of Character Group and initialize Local location.
        //costume2.transform.parent = characterGroup;
        costume2.transform.localPosition = Vector3.zero;

        //Synchronize bone movements.
        costume2.Synchronize(mainCharacter, false, false, false, true, SYNC_BONE_OPTION.MatchFromRoot);

        //Modify the sorting order of the Costume 1 meshes based on the meshes of the Main Character.
        //Scarf
        costume2.SetSortingOrder("helmet", mainCharacter.GetSortingOrder("Nose") + 1);
        costume2.SetSortingOrder("chest", mainCharacter.GetSortingOrder("Torso") + 1);

        //ArmBraces
        costume2.SetSortingOrder("shoulderpadR", mainCharacter.GetSortingOrder("RPaw") + 1);
        costume2.SetSortingOrder("shoulderpadL", mainCharacter.GetSortingOrder("LPaw") + 1);
        costume2.SetSortingOrder("tail", mainCharacter.GetSortingOrder("Tail") + 1);

        //Kneepads
        costume2.SetSortingOrder("footarmourR", mainCharacter.GetSortingOrder("RFoot") + 1);
        costume2.SetSortingOrder("footarmourL", mainCharacter.GetSortingOrder("LFoot") + 1);
    }

    private void DetachCostume1()
    {
        //Unsynchronize the bones by calling the Unsynchronize function.
        costume1.Unsynchronize();

        //Separates the costumes from the parent GameObject.
        //(Positions are assigned to be nicely distributed appropriately.)
        costume1.transform.parent = null;
        costume1.transform.position = new Vector3(0, 0, 0);
    }

    private void DetachCostume1Parts()
    {

    }

    private void DetachCostume2()
    {
        //Unsynchronize the bones by calling the Unsynchronize function.
        costume2.Unsynchronize();

        //Separates the costumes from the parent GameObject.
        //(Positions are assigned to be nicely distributed appropriately.)
        costume2.transform.parent = null;
        costume2.transform.position = new Vector3(4, 0, 0);
    }
}
