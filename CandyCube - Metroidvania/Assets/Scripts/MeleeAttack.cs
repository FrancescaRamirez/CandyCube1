using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeAttack : MonoBehaviour
{
    public float cooldownDuration = 0.2f; //Time until can attack again
    float cooldownTimer = 1;
    public int damage; //How much damage the attack should transmit
    public float attackDuration; //How long the attack is
    float attackTimer; //Timer to count how long the attack has been out
    public bool attacking; //Are we attacking right now?
    
    public GameObject attackStuff; //All the colliders included in the attack

    bool player = false; //Is this the player or the enemy

    private void Start()
    {
        attackTimer = attackDuration; //Reset the timer
        cooldownTimer = cooldownDuration;
    }

    private void Update()
    {
        if (attacking && attackTimer > 0) //If the attack is currently out
        {
            attackTimer -= Time.deltaTime;
        }
        else if(attacking && cooldownTimer > 0) //If the attack is on cooldown
        {
            DisableAttack();
            cooldownTimer -= Time.deltaTime;
            attacking = false;
        }
        else //Otherwise sheath the sword sir
        {
            cooldownTimer = cooldownDuration;
        }
    }

    public void Attack()
    {
        if(!attacking)
        {
            attackTimer = attackDuration;
            attacking = true;
            EnableAttack();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        print("Object : " + collision.name + "| Tag : " + collision.tag + "| Health script : " + collision.GetComponent<Health>());

        if((player && collision.tag == "Enemy") && collision.GetComponent<Health>())
        {
            Health theirHealth = collision.GetComponent<Health>();
            theirHealth.TakeDamage(damage);
        }
        if ((!player && collision.tag == "Player") && collision.GetComponent<Health>())
        {
            Health theirHealth = collision.GetComponent<Health>();
            theirHealth.TakeDamage(damage);
        }
    }

    void DisableAttack()
    {
        attackStuff.SetActive(false);
    }

    void EnableAttack()
    {
        attackStuff.SetActive(true);
    }
}
