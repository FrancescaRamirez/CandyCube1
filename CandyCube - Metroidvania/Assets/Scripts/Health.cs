using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AnyPortrait;
using UnityEngine.UI;


public class Health : MonoBehaviour
{
    int maximumHealth;
    public int currentHealth;
    float hitCooldownTimer = 0;
    public float hitCooldownMaximum;

    public SpriteRenderer sr;
    Color hitColor;
    Color startColor;

    public apPortrait ap;

    public int boneCount = 1;
    public GameObject deathParticle;

    public GameObject bone;

    public float maxBoneVelocity = 16;
    public float minBoneVelocity = 12;

    public List<GameObject> respawnPoints;
    GameObject closestRespawnPoint;
    public GameObject respawnParticles;

    public Slider healthBar;

    public ParticleSystem DamageParticle;

    public float deathFreezeDuration = 0;
    public float deathFreezeScale = 0;

    private void Start()
    {
        maximumHealth = currentHealth;

        if (gameObject.GetComponent<SpriteRenderer>()) //Look for a sprite
        {
            sr = gameObject.GetComponent<SpriteRenderer>();
        }
        else if (gameObject.GetComponentInChildren<SpriteRenderer>())
        {
            sr = gameObject.GetComponentInChildren<SpriteRenderer>();
        }
   
        if(sr != null)
        {
            startColor = sr.color;
            hitColor = new Color(sr.color.r - 50, sr.color.g - 50, sr.color.b - 50);
        }
        
    }

    private void Update()
    {
        if (hitCooldownTimer > 0)
        {
            hitCooldownTimer -= Time.deltaTime;
        }
        else if(sr == null) { }
        else if(sr.color != startColor)
        {
            sr.color = startColor; 
        }
    }

    public void TakeDamage(int damage)
    {
        if(hitCooldownTimer <= 0)
        {
            currentHealth -= damage;
            hitCooldownTimer = hitCooldownMaximum;

            DamageParticle.Play();
        }

        if(this.tag == "Enemy" && currentHealth <= 0)
        {
            if (deathParticle)
            {
                Player.instance.timeManager.freezeEvent(deathFreezeDuration, deathFreezeScale); //Freeze frame
                Instantiate(deathParticle, this.gameObject.transform.position, Random.rotation); //Make a new randomly rotated death particle if there is one
                DamageParticle.Play();
            }
            if(boneCount > 0)
            {
                Rigidbody2D currentBone;
                for (int i = 0; i < boneCount; i++)
                {
                    currentBone = Instantiate(bone, this.gameObject.transform.position, Quaternion.identity).GetComponent<Rigidbody2D>();
                    currentBone.velocity = new Vector3((Random.value < .5? 1 : -1) * Random.Range(minBoneVelocity, maxBoneVelocity), Random.Range(minBoneVelocity, maxBoneVelocity), 0);
                    currentBone.transform.Rotate(0, 0, Random.Range(0, 360));
                }
            }

            if (this.gameObject.GetComponent<BossManager>() != null) //if this is a boss
            {
                this.gameObject.GetComponent<BossManager>().BossDeath(); //Kill the boss
            }
            
            Destroy(this.gameObject);
        }

    }

}
