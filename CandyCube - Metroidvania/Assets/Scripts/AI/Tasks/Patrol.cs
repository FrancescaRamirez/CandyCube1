﻿using BehaviorDesigner.Runtime.Tasks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Scripts.AI
{
    public class Patrol : EnemyAction
    {
        public bool looping;

        public List<GameObject> wayPoints;

        [BehaviorDesigner.Runtime.Tasks.Tooltip("Returns a success if the patrol is finished. (Only if not infinite)")]
        public int patrolsToSuccess = 1;
        int currentPatrols;

        int currentWaypoint;

        public float accelerationForce = 10f;

        public float distanceToWaypoint = 2;

        bool moving;


        public override void OnStart()
        {
            currentWaypoint = 0;
            FaceWaypoint();
            if (patrolsToSuccess > 0)
            currentPatrols = 0;
        }

        public override void OnFixedUpdate()
        {
            if (moving)
            {
                var direction = wayPoints[currentWaypoint].transform.position.x < transform.position.x ? 1 : -1;
                body.AddForce((Vector3.left * direction) * accelerationForce, ForceMode2D.Force);
            }
        }

        //While taskstatus is returned running it will keep playing this one
        public override TaskStatus OnUpdate()
        {

            if(Vector2.Distance(this.transform.position, wayPoints[currentWaypoint].transform.position) < distanceToWaypoint)
            {
                moving = false;

                //body.velocity = Vector2.zero;
                currentWaypoint++;
                currentPatrols++;
                if (wayPoints.Count < currentWaypoint+1) //If the next waypoint isnt real
                {
                    if (!looping || (patrolsToSuccess > 0 && currentPatrols > patrolsToSuccess)) //If its not supposed to keep going at end or theres no patrol limit
                    {
                        currentWaypoint = 0;
                        FaceWaypoint();
                        return TaskStatus.Success;
                    }
                    else
                    {
                        currentWaypoint = 0;
                    }
                }
                
                FaceWaypoint();
            }
            else
            {
                moving = true;
            }

            return TaskStatus.Running;
        }

        public void FaceWaypoint()
        {
            var direction = wayPoints[currentWaypoint].transform.position.x < transform.position.x ? -1 : 1;

            if (direction == -1 && this.transform.localScale.x < 0) //If the current waypoint is on the left and not facing left
            {
                this.transform.localScale = new Vector3(Mathf.Abs(this.transform.localScale.x), this.transform.localScale.y, this.transform.localScale.z); //Turn to the left
            }
            else if (direction == 1 && this.transform.localScale.x > 0) //If the current waypoint is on the right and not facing right
            {
                this.transform.localScale = new Vector3(-this.transform.localScale.x, this.transform.localScale.y, this.transform.localScale.z); //Turn to the right
            }
        }
    }
}