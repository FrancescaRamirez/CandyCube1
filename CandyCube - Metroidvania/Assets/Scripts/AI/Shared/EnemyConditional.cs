﻿using BehaviorDesigner.Runtime.Tasks;

using UnityEngine;

namespace Scripts.AI
{
    public class EnemyConditional : Conditional
    {
        protected Rigidbody2D body;
        protected Animator animator;
        protected GameObject EnemyVisuals;
        //Assign here the thing that lets us flash for damage

        public override void OnAwake()
        {
            body = GetComponent<Rigidbody2D>();
            animator = GetComponent<Animator>();
            EnemyVisuals = animator.gameObject;
        }
    }
}
