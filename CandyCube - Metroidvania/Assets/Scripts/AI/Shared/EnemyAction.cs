﻿using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;

namespace Scripts.AI
{
    public class EnemyAction : Action
    {
        protected Rigidbody2D body;
        protected Animator animator;
        protected GameObject enemyVisuals;
        //Assign here the thing that lets us flash for damage

        public override void OnAwake()
        {
            body = GetComponent<Rigidbody2D>();
            animator = gameObject.GetComponentInChildren<Animator>();
            enemyVisuals = animator.gameObject;
        }
    }
}
