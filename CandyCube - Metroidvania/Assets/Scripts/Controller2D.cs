﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Controller2D : RaycastController
{ 
    public float maxSlopeAngle = 80;

    public CollisionInfo collisions;

    public float fallingThroughDuration = 0.2f;

    [HideInInspector]
    public Vector2 playerInput;


    public override void Start()
    {
        //Calls the raycast controllers start method
        base.Start();
        collisions.faceDir = 1;
    }

    //Override that nullifies input for calls outside class
    public void Move(Vector2 moveAmount, bool standingOnPlatform)
    {
        Move(moveAmount, Vector2.zero, standingOnPlatform);
    }

    public void Move(Vector2 moveAmount, Vector2 input, bool standingOnPlatform = false)
    {
        UpdateRaycastOrigins();
        collisions.Reset();
        collisions.moveAmountOld = moveAmount;
        //Assign input
        playerInput = input;

        if (moveAmount.y < 0)
        {
            DescendSlope(ref moveAmount);
        }

        //Changes the facing direction if moving in a way
        if (moveAmount.x != 0)
        {
            collisions.faceDir = (int)Mathf.Sign(moveAmount.x);
        }

        //Gives the moveAmount to vertical and horizontal collisions to be adjusted
        //Always be doing horizontal collisions (for wallsliding)
        HorizontalCollisions(ref moveAmount);

        if(moveAmount.y != 0)
        {
            VerticalCollisions(ref moveAmount);
        }

        transform.Translate(moveAmount);

        if(standingOnPlatform == true)
        {
            collisions.below = true;
        }
    }

    void HorizontalCollisions(ref Vector2 moveAmount)
    {
        //Get which direction it's moving in
        float directionX = collisions.faceDir;
        float rayLength = Mathf.Abs(moveAmount.x) + skinWidth;

        if(Mathf.Abs(moveAmount.x) < skinWidth)
        {
            rayLength = 2 * skinWidth;
        }

        for (int i = 0; i < horizontalRayCount; i++)
        {
            //If the direction is moving left assign the origin to start drawing rays from to bottom left. Otherwise from bottom right
            Vector2 rayOrigin = (directionX == -1) ? raycastOrigins.bottomLeft : raycastOrigins.bottomRight;
            //Calculates the ray from where it will be taking into account the x movement
            rayOrigin += Vector2.up * (horizontalRaySpacing * i);
            //Shoots the raycast for the current ray
            RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.right * directionX, rayLength, collisionMask);

            Debug.DrawRay(rayOrigin, Vector2.right * directionX, Color.green);

            if (hit)
            {
                if(hit.distance == 0)
                {
                    continue;
                }

                float slopeAngle = Vector2.Angle(hit.normal, Vector2.up);
                
                if(i == 0 && slopeAngle <= maxSlopeAngle)
                {
                    float distanceToSlopeStart = 0;
                    //If starting to climb a new slope
                    if(slopeAngle != collisions.slopeAngleOld)
                    {
                        //Makes sure it's sitting against the slope
                        distanceToSlopeStart = hit.distance - skinWidth;
                        moveAmount.x -= distanceToSlopeStart * directionX;
                    }
                    ClimbSlope(ref moveAmount, slopeAngle);
                    //Adds the distance back on after the climbing of the slope is done
                    moveAmount.x += distanceToSlopeStart * directionX;
                }

                //If not climbing already or unclimable surface then check the rest of the array for collisions
                if (!collisions.climbingSlope || slopeAngle > maxSlopeAngle)
                {
                    //Resets the moveAmount change if your descending from one slope onto a rising slope
                    if (collisions.descendingSlope)
                    {
                        collisions.descendingSlope = false;
                        moveAmount = collisions.moveAmountOld;
                    }
                    moveAmount.x = (hit.distance - skinWidth) * directionX;
                    //Re-assign the raylength to the collision so the next check doesn't hit something further away
                    rayLength = hit.distance;

                    //Edge case of something being in the path that blocks the sideways rays on a slope. Updates the y moveAmount using trig
                    if (collisions.climbingSlope)
                    {
                        moveAmount.y = Mathf.Tan(collisions.slopeAngle * Mathf.Deg2Rad * Mathf.Abs(moveAmount.x));
                    }

                    //Sets collision.left true if going left
                    collisions.left = directionX == -1;
                    collisions.right = directionX == 1;
                }

            }
        }
    }

    void VerticalCollisions(ref Vector2 moveAmount)
    {
        //Get which direction it's moving in
        float directionY = Mathf.Sign(moveAmount.y);
        float rayLength = Mathf.Abs(moveAmount.y) + skinWidth;

        for (int i = 0; i < verticalRayCount; i++)
        {
            //If the direction is moving down assign the origin to start drawing rays from to bottom left. Otherwise top left
            Vector2 rayOrigin = (directionY == -1) ? raycastOrigins.bottomLeft : raycastOrigins.topLeft;
            //Calculates the ray from where it will be taking into account the x movement
            rayOrigin += Vector2.right * (verticalRaySpacing * i + moveAmount.x);
            //Shoots the raycast for the current ray
            RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.up * directionY, rayLength, collisionMask);

            Debug.DrawRay(rayOrigin, Vector2.up * directionY, Color.green);

            if (hit)
            {
                //If it's a platform you move through
                if(hit.collider.tag == "Through")
                {
                    //Moving up
                    if (directionY == 1 || hit.distance == 0)
                    {
                        //Skip colliding with it on the way up
                        continue;
                    }
                    //If pressing down fall through platform
                    if (collisions.fallingThroughPlatform)
                    {
                        continue;
                    }
                    if(playerInput.y == -1)
                    {
                        collisions.fallingThroughPlatform = true;
                        Invoke("ResetFallingThroughPlatform", fallingThroughDuration);
                        continue;
                    }
                    
                }
                moveAmount.y = (hit.distance - skinWidth) * directionY;
                //Re-assign the raylength to the collision so the next check doesn't hit something further away
                rayLength = hit.distance;

                //If on a slope updates the x moveAmount so that if there is a collision above the gameobject it doesn't try to keep updating the x
                if (collisions.climbingSlope)
                {
                    moveAmount.x = moveAmount.y / Mathf.Tan(collisions.slopeAngle * Mathf.Deg2Rad) * Mathf.Sign(moveAmount.x);
                }

                //Sets the collision in the direction its colliding to true
                collisions.below = directionY == -1;
                collisions.above = directionY == 1;
            }
        }

        //This check is for if your changing to a new slope to make sure you transition without stuttering
        if (collisions.climbingSlope)
        {
            float directionX = Mathf.Sign(moveAmount.x);
            rayLength = Mathf.Abs(moveAmount.x) + skinWidth;
            Vector2 rayOrigin = ((directionX == -1) ? raycastOrigins.bottomLeft : raycastOrigins.bottomRight) + Vector2.up * moveAmount.y;
            RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.right * directionX, rayLength, collisionMask);

            if (hit)
            {
                float slopeAngle = Vector2.Angle(hit.normal, Vector2.up);
                if(slopeAngle != collisions.slopeAngle)
                {
                    moveAmount.x = (hit.distance - skinWidth) * directionX;
                    collisions.slopeAngle = slopeAngle;
                }
            }
        }
    }

    void ClimbSlope(ref Vector2 moveAmount, float slopeAngle)
    {
        float moveDistance = Mathf.Abs(moveAmount.x);

        //Use some trigonometry to find the amount of x and y moveAmount to maintain full speed up the slope
        //moveAmount y = sin(theta) * long side of triangle
        float climbmoveAmountY = Mathf.Sin(slopeAngle * Mathf.Deg2Rad) * moveDistance;
        
        if (moveAmount.y <= climbmoveAmountY)
        {
            moveAmount.y = climbmoveAmountY;
            //moveAmount x = cos(theta) * long side of trangle + sign of direction moving in (this is just climbing slopes so moveAmount y is only up)
            moveAmount.x = Mathf.Cos(slopeAngle * Mathf.Deg2Rad) * moveDistance * Mathf.Sign(moveAmount.x);
            //Can assume on the ground because climbing slope
            collisions.below = true;
            //Updates the collision struct with the slope variables
            collisions.climbingSlope = true;
            collisions.slopeAngle = slopeAngle;
        }

    }

    void DescendSlope(ref Vector2 moveAmount)
    {

        RaycastHit2D maxSlopeHitLeft = Physics2D.Raycast(raycastOrigins.bottomLeft, Vector2.down, Mathf.Abs(moveAmount.y) + skinWidth, collisionMask);
        RaycastHit2D maxSlopeHitRight = Physics2D.Raycast(raycastOrigins.bottomRight, Vector2.down, Mathf.Abs(moveAmount.y) + skinWidth, collisionMask);
        SlideDownMaxSlope(maxSlopeHitLeft, ref moveAmount);
        SlideDownMaxSlope(maxSlopeHitRight, ref moveAmount);

        //If your not moving down max slope then calculate the normal slope
        if (!collisions.slidingDownMaxSlope)
        {
            float directionX = Mathf.Sign(moveAmount.x);
            //Depending on which direction your moving it casts down on that side
            Vector2 rayOrigin = (directionX == -1) ? raycastOrigins.bottomRight : raycastOrigins.bottomLeft;
            RaycastHit2D hit = Physics2D.Raycast(rayOrigin, -Vector2.up, Mathf.Infinity, collisionMask);

            //If you hit then it gets the angle of the slope your headed onto
            if (hit)
            {
                float slopeAngle = Vector2.Angle(hit.normal, Vector2.up);
                if (slopeAngle != 0 && slopeAngle <= maxSlopeAngle)
                {
                    //If the hit is in the direction your moving(going down slope)
                    if (Mathf.Sign(hit.normal.x) == directionX)
                    {

                        if (hit.distance - skinWidth <= Mathf.Tan(slopeAngle * Mathf.Deg2Rad) * Mathf.Abs(moveAmount.x))
                        {
                            //Calculates the decrease in a similar way to how it calculated it in slope climb
                            float moveDistance = Mathf.Abs(moveAmount.x);
                            float descendmoveAmountY = Mathf.Sin(slopeAngle * Mathf.Deg2Rad) * moveDistance;
                            moveAmount.x = Mathf.Cos(slopeAngle * Mathf.Deg2Rad) * moveDistance * Mathf.Sign(moveAmount.x);
                            moveAmount.y -= descendmoveAmountY;

                            collisions.slopeAngle = slopeAngle;
                            collisions.descendingSlope = true;
                            collisions.below = true;
                        }
                    }
                }
            }
        }
    }

    void SlideDownMaxSlope(RaycastHit2D hit, ref Vector2 moveAmount)
    {
        if (hit)
        {
            float slopeAngle = Vector2.Angle(hit.normal, Vector2.up);
            if(slopeAngle > maxSlopeAngle)
            {
                //Hit normal.x is the direction the slope is facing based on its normal. The equation using tan is to determine how mych you slide
                moveAmount.x = hit.normal.x * (Mathf.Abs(moveAmount.y - hit.distance)) / Mathf.Tan(slopeAngle * Mathf.Deg2Rad);

                collisions.slopeAngle = slopeAngle;
                collisions.slidingDownMaxSlope = true;
            }
        }
    }

    void ResetFallingThroughPlatform()
    {
        collisions.fallingThroughPlatform = false;
    }

    public struct CollisionInfo
    {
        public bool above, below;
        public bool left, right;

        public bool descendingSlope;
        public bool climbingSlope;
        public bool slidingDownMaxSlope;

        public float slopeAngle, slopeAngleOld;
        public Vector2 moveAmountOld;
        public int faceDir;
        public bool fallingThroughPlatform;

        //Resets the collision info
        public void Reset()
        {
            above = below = false;
            left = right = false;
            climbingSlope = false;
            descendingSlope = false;
            slidingDownMaxSlope = false;

            //Sets the slope angle old to the slope angle of the old frame before resetting for the current frame
            slopeAngleOld = slopeAngle;
            slopeAngle = 0;
        }
    }
}
