using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimedShot : MonoBehaviour
{
    GameObject player;
    Vector3 playerOldPosition;

    Rigidbody2D rb;

    public float forceAmount = 150;
    public float yOffset = 2;

    public GameObject explosionEffectPrefab;

    public AK.Wwise.Event destroy;
    public GameObject thisWiseEvent;

    private void Start()
    {
        player = Player.instance.gameObject;
        playerOldPosition = player.transform.position;
        rb = gameObject.GetComponent<Rigidbody2D>();

        //Movement code
        playerOldPosition = new Vector3(playerOldPosition.x, playerOldPosition.y + yOffset, playerOldPosition.z);
        Vector2 forceDirection = playerOldPosition - transform.position;
        rb.AddForce((new Vector2(forceDirection.x, forceDirection.y)) * forceAmount);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player") //If it's player inflict damage
        {
            Player.instance.playerHealth.TakeDamage(2);
        }

        if (collision.gameObject.tag != "Enemy") //If it's not enemy destroy itself on object
        {
            Instantiate(explosionEffectPrefab, transform.position, Quaternion.identity);
            destroy.Post(thisWiseEvent);
            Destroy(this.gameObject);
        }
    }
}
