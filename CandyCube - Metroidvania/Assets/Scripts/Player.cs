﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(Controller2D))]
public class Player : MonoBehaviour
{
    public static Player instance;

    public GameObject playerSprite;
    public PlayerAttack playerAttack;

    public bool facingDirectionLeft;

    public Animator playerAnim;

    //Variables used to calculate gravity and jump velocity
    public float maxJumpHeight = 4;
    public float minJumpHeight = 1;
    public float timeToJumpApex = .6f;
    public float accelerationTimeAirborne = .2f;
    public float accelerationTimeGrounded = .1f;
    public float reverseAccelerationTimeAirborne = 0.8f; //For when there is no input (zeroing out velocity, or input in the opposite direction we are currently going). 
    public float reverseAccelerationTimeGrounded = 0.5f;

    public float wallSlideSpeedMax = 9;
    public float wallSlideSpeed = 3; ///Needs to replace max and slowly increase to it the longer sliding
    float wallSlideSpeedMinimum;
    public float wallSlideSpeedMultiplier = 10;
    public Vector2 wallJumpClimb;
    public Vector2 wallJumpOff;
    public Vector2 wallLeap;
    public float wallStickTime = 0.25f;
    float wallUnstickTimer;
    float wallJumpCoyoteTimerMax = 0.2f;
    public float wallJumpCoyoteTimer = 0.2f; // to be the amount of time they can still walljump after unsticking
    bool lastWallLeft = true;
    float numberOfWalljumps = 0; ///needs to be the number of walljumpclimbs since last grounded or wallleap
    public bool wallJumpUnlocked = false;
    public float verticalWallJumpDebuff = 0;
    public float maxWallJumpDebuff = 3;
    public float wallJumpDebuffRate = 0.5f;

    float gravity;
    float maxJumpVelocity;
    float minJumpVelocity;
    float velocityXSmoothing;
    Vector3 velocity;

    Controller2D controller;

    public float moveSpeed = 2;

    Vector2 directionalInput;

    bool wallSliding;
    int wallDirX;

    [NonSerialized]
    public PlayerHealth playerHealth;
    public bool attacking;
    public BoneArmorManager bam;
    public PlayerInput playerInput;
    public GameObject PlayerAudioManager;

    public CameraFollow cameraFollow;

    public BoxCollider2D bc;

    public bool doubleJumpUnlocked = true; //Tracks if the double jump is unlocked
    bool doubleJumped = false; //Keeps track of if they have double jumped
    public float maxDoubleJumpHeight = 2; //Set the same for fixed jump height with no variablity
    public float minDoubleJumpHeight = 1;
    public float doubleJumpTimeToJumpApex = 0.2f;
    float doubleJumpMaxVelocity;
    float doubleJumpMinVelocity;
    float doubleJumpGravity;

    public bool dashUnlocked = true;
    public bool dashing = false;
    public float dashDuration = 1;
    public float dashSpeed = 1;
    public float dashCooldownDuration = 1;
    float dashCooldownTimer;
    public float teleportDashDistance = 10;

    public bool teleportDash;

    public ParticleSystem WJPart;
    public ParticleSystem DashPart;
    public ParticleSystem DJPart;

    public bool boneArmorUnlocked = false;
    public bool barkUnlocked = false;

    public TimeManager timeManager;

    public GameObject teleportScreen;

    public LoadingManager loadingManager;

    public InteractiveMusic interactiveMusic;

    public PlayerParent pp;

    public bool dead = false;

    public AK.Wwise.Event jump;
    public AK.Wwise.Event land;
    public AK.Wwise.Event footsteps;
    public AK.Wwise.Event dash;
    public AK.Wwise.Event doubleJump;

    bool airborneLastFrame = true;

    void Awake()
    {
        if (Player.instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            Player.instance = this;
        }

        wallSlideSpeedMinimum = wallSlideSpeed;
        bc = GetComponent<BoxCollider2D>();
        playerHealth = GetComponent<PlayerHealth>();
        controller = GetComponent<Controller2D>();
        playerAnim = playerSprite.GetComponent<Animator>();
        bam = GetComponent<BoneArmorManager>();
        playerInput = GetComponent<PlayerInput>();
        playerAttack = GetComponent<PlayerAttack>();
        timeManager = GetComponent<TimeManager>();
        loadingManager = GetComponent<LoadingManager>();

        timeManager.ResumeGame();

        //Rearranged kinematic equations to get gravity and jump velocity from time to jump apex and jumpheight
        gravity = -(2 * maxJumpHeight) / Mathf.Pow(timeToJumpApex, 2);
        maxJumpVelocity = Mathf.Abs(gravity) * timeToJumpApex; //Sets normal jump velocity variables
        minJumpVelocity = Mathf.Sqrt(2 * Mathf.Abs(gravity) * minJumpVelocity);

        doubleJumpGravity = -(2 * maxDoubleJumpHeight) / Mathf.Pow(doubleJumpTimeToJumpApex, 2);
        doubleJumpMaxVelocity = Mathf.Abs(doubleJumpGravity) * doubleJumpTimeToJumpApex; //Sets double jump velocity variables
        doubleJumpMinVelocity = Mathf.Sqrt(2 * Mathf.Abs(doubleJumpGravity) * doubleJumpMinVelocity);

        dashCooldownTimer = dashCooldownDuration;
    }

    private void Update()
    {
        if (airborneLastFrame && controller.collisions.below)
        {
            land.Post(PlayerAudioManager);
            airborneLastFrame = false;
        }
        else if(!airborneLastFrame && !controller.collisions.below)
        {
            airborneLastFrame = true;
        }

        if (!playerAnim.GetCurrentAnimatorStateInfo(0).IsName("Walking"))
        {
            footsteps.Stop(PlayerAudioManager);
        }

        if(verticalWallJumpDebuff > maxWallJumpDebuff)
        {
            verticalWallJumpDebuff = maxWallJumpDebuff;
        }
        
        if(verticalWallJumpDebuff > 1 && !wallSliding) //If theres walljump debuff built up
        {
            verticalWallJumpDebuff -= Time.deltaTime;
        }
        
        if(verticalWallJumpDebuff < 1) //If walljump debuff is negative after the last change
        {
            verticalWallJumpDebuff = 1;
        }

        CalculateVelocity();

        if (wallJumpUnlocked)
        {
            HandleWallSliding();
        }

        if(wallJumpCoyoteTimer > 0 && !wallSliding) //If not on the wall and there is coyote time left from the wall
        {
            wallJumpCoyoteTimer -= Time.deltaTime; //Count down the timer
        }

        if (!wallSliding) //Reset the wallslide speed when not wallsliding
        {
            wallSlideSpeed = wallSlideSpeedMinimum;
        }

        controller.Move(velocity * Time.deltaTime, directionalInput);


        if(dashCooldownTimer > 0 && !dashing) //If not currently dashing the dash cooldown will count down
        {
            dashCooldownTimer -= Time.deltaTime;
        }

        //If collision above or below reset velocity on the Y.
        if (controller.collisions.above || controller.collisions.below)
        {
            //If sliding down max slope don't reset velocity because of thing beneath
            if (!controller.collisions.slidingDownMaxSlope)
            {
                velocity.y = 0;
            }
        }

        if (controller.collisions.below) //If it has grounded 
        {
            doubleJumped = false; //it resets the double jump value

            //TRIGGER THE APPROPRIATE ANIMS (if they haven't been triggered)
            if (velocity.magnitude > 0.25f && !playerAnim.GetCurrentAnimatorStateInfo(0).IsName("Walking") && !playerAnim.GetCurrentAnimatorStateInfo(0).IsName("Attacking") && !dead) 
            {
                playerAnim.SetTrigger("Walking");
                footsteps.Post(PlayerAudioManager);

            }
            else if(velocity.magnitude <= 0.25f && !playerAnim.GetCurrentAnimatorStateInfo(0).IsName("Idle") && !playerAnim.GetCurrentAnimatorStateInfo(0).IsName("Attacking") && !dead)
            {
                playerAnim.SetTrigger("Idle");
            }
        }
    }

    public void OnDashInputDown()
    {
        if (dashCooldownTimer < 0 && dashUnlocked) //If the dash cooldown is over
        {
            dash.Post(PlayerAudioManager);
            StartCoroutine(DashTimer(dashSpeed, dashDuration));
            dashCooldownTimer = dashCooldownDuration; //Reset dash cooldown
            dashing = true;
        }
    }

    public void SetDirectionalInput(Vector2 input)
    {
        directionalInput = input;
    }

    public void OnJumpInputDown()
    {
            //Wall Jumps
            if (wallSliding && wallJumpUnlocked)
            { //Moving in same direction as the wall facing

                jump.Post(Player.instance.PlayerAudioManager);


                if (!Player.instance.playerAnim.GetCurrentAnimatorStateInfo(0).IsName("WallJump") && !dead)
                {
                    playerAnim.SetTrigger("WallJump");

                    WJPart.Play();
                }

                if (Mathf.Sign(wallDirX) == Mathf.Sign(directionalInput.x)) //Do the wall jump climb if input toward wall
                {
                    velocity.x = -wallDirX * wallJumpClimb.x;
                    velocity.y = wallJumpClimb.y / verticalWallJumpDebuff; //Jump up by an amount of the climb force divided by the current debuff amount
                    verticalWallJumpDebuff += wallJumpDebuffRate;
                    wallJumpCoyoteTimer = 0;
                }
                else if (directionalInput.x == 0) //If no x input on jump
                {
                    if(directionalInput.y > 0.5f) //If the player is pushing up input do the wall jump climb
                    {
                        velocity.x = -wallDirX * wallJumpClimb.x;
                        velocity.y = wallJumpClimb.y / verticalWallJumpDebuff; //Jump up by an amount of the climb force divided by the current debuff amount
                        verticalWallJumpDebuff += wallJumpDebuffRate;
                        wallJumpCoyoteTimer = 0;
                    }
                    else //Fall off wall if not trying to jump up
                    {
                        //velocity.x = -wallDirX * wallJumpOff.x;
                        //velocity.y = wallJumpOff.y;

                        velocity.x = -wallDirX * wallLeap.x;
                        velocity.y = wallLeap.y;
                    }
                    
                }
                else //This case is if the input is opposite the wall, leap off
                {
                    velocity.x = -wallDirX * wallLeap.x;
                    velocity.y = wallLeap.y;
                }
            }
            

            if (controller.collisions.below) //Regular Jumps
            {
                velocity.y = maxJumpVelocity;

                jump.Post(Player.instance.PlayerAudioManager);

                if (!Player.instance.playerAnim.GetCurrentAnimatorStateInfo(0).IsName("Attacking") && !dead)
                {
                    playerAnim.SetTrigger("Jumping");
                }
            }
            else if (wallJumpCoyoteTimer > 0 && !wallSliding && wallJumpUnlocked)
            {
                jump.Post(Player.instance.PlayerAudioManager);

                wallJumpCoyoteTimer = 0;
                velocity.x = -(lastWallLeft ? -1 : 1) * wallLeap.x;
                velocity.y = wallLeap.y;
            }
            else if(!doubleJumped && doubleJumpUnlocked && !wallSliding) //If the player hasn't double jumped yet, and they have the ability to double jump
            {
                doubleJump.Post(PlayerAudioManager);
                doubleJumped = true; //Set double jumped to true
                velocity.y = doubleJumpMaxVelocity;

                DJPart.Play();

                if (!Player.instance.playerAnim.GetCurrentAnimatorStateInfo(0).IsName("Attacking") && !dead)
                {
                    playerAnim.SetTrigger("Jumping");
                }
            }
    }
    
    public void OnJumpInputUp()
    {
        if (velocity.y > minJumpVelocity)
        {
            velocity.y = minJumpVelocity;
        }
    }

    void CalculateVelocity()
    {
        //Calculates horizontal movement
        float targetVelocityX = directionalInput.x * moveSpeed;

        //print(velocity.x);
        if(Mathf.Sign(directionalInput.x) != Mathf.Sign(velocity.x) || directionalInput.x == 0) //For when we are zeroing out velocity (either input in the opposite direction we are headed or no input)
        {
            velocity.x = Mathf.SmoothDamp(velocity.x, targetVelocityX, ref velocityXSmoothing, (controller.collisions.below) ? reverseAccelerationTimeGrounded : reverseAccelerationTimeAirborne);
        }
        else //For when there is input 
        {
            velocity.x = Mathf.SmoothDamp(velocity.x, targetVelocityX, ref velocityXSmoothing, (controller.collisions.below) ? accelerationTimeGrounded : accelerationTimeAirborne);
        }

        velocity.y += gravity * Time.deltaTime;
    }

    void HandleWallSliding()
    {
        wallDirX = (controller.collisions.left) ? -1 : 1;
        wallSliding = false;

        //Checks if the player is wallsliding
        if ((controller.collisions.left || controller.collisions.right) && !controller.collisions.below && velocity.y < 0)
        {

            if (!Player.instance.playerAnim.GetCurrentAnimatorStateInfo(0).IsName("WallSlide") && !dead)
            {
                playerAnim.SetTrigger("WallSlide");
            }

            wallSliding = true;

            lastWallLeft = wallDirX == -1 ? true : false; //Stores the last wall we contacted
            wallJumpCoyoteTimer = wallJumpCoyoteTimerMax; //Reset the coyot time on walljumps

            if(wallSlideSpeed < wallSlideSpeedMax) //If wallslide hasn't reached max yet
            {
                wallSlideSpeed += Time.deltaTime * wallSlideSpeedMultiplier;
            }
            else if(wallSlideSpeed > wallSlideSpeedMax) //If wallslide is at max
            {
                wallSlideSpeed = wallSlideSpeedMax;
            }

            if (velocity.y < -wallSlideSpeed) //If the y velocity is faster than the slide
            {
                velocity.y = -wallSlideSpeed;
            }

            if (wallUnstickTimer > 0)
            {
                //Reset the smoothing to avoid weird bugs
                velocityXSmoothing = 0;
                //Reset velocity
                velocity.x = 0;

                //If trying to get off the wall count down timer
                if (directionalInput.x != wallDirX && directionalInput.x != 0)
                {
                    wallUnstickTimer -= Time.deltaTime;
                }
                else
                {   //Reset timer if input is dropped again (not trying to get off the wall anymore)
                    wallUnstickTimer = wallStickTime;
                }
            }
            else
            { //if still on the wall but timer is 0 for some reason restart timer
                wallUnstickTimer = wallStickTime;
            }
        }
    }

    IEnumerator DashTimer(float speed, float duration)
    {
        float time = 0;

        if (teleportDash) //Does a teleport dash
        {
            controller.Move(new Vector2(facingDirectionLeft ? -teleportDashDistance : teleportDashDistance, 0), false); //Dash directionaly based on facing direction

            

            yield return null;
        }
        else //Does a normal dash
        {
            while (time < duration)
            {
                controller.Move(new Vector2(facingDirectionLeft ? -speed : speed, 0), false); //Dash directionaly based on facing direction
                time += Time.fixedDeltaTime;

                DashPart.Play();

                yield return null;
            }
        }

        dashing = false; //When it's done dashing tell everybody so player can move again
    }



}
