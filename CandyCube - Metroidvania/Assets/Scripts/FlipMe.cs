using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlipMe : MonoBehaviour
{
    public GameObject player;
    public float startScale;

    private void Start()
    {
        player = Player.instance.gameObject;
        startScale = transform.localScale.x;
    }

    private void Update()
    {
        if (player.transform.position.x < transform.position.x && transform.localScale.x != -startScale)
        {
            transform.localScale = new Vector3(-startScale, transform.localScale.y);
        }
        else if (player.transform.position.x > transform.position.x && transform.localScale.x != startScale)
        {
            transform.localScale = new Vector3(startScale, transform.localScale.y);
        }
    }

}
