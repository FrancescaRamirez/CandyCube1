﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using InControl;
using PixelCrushers.DialogueSystem;
using UnityEngine.UI;
using UnityEngine.EventSystems;

[RequireComponent(typeof(Player))]
public class PlayerInput : MonoBehaviour
{
    Player player;

    public bool recievingInput = true;

    public bool recievingMovementInput = true;

    public SpriteRenderer sr;

    public Controller2D controller;

    InputDevice inputDevice;

    bool facingLeft;

    public Vector2 directionalInput;

    public QuestLogWindowHotkey closeQuestButton;

    public Button continueButton;

    public AK.Wwise.Event openJournal;
    public AK.Wwise.Event closeJournal;

    public GameObject settingsObject;

    private void Start()
    {
        player = Player.instance;
        controller = transform.GetComponent<Controller2D>();
        sr = GetComponentInChildren<SpriteRenderer>();
    }

    private void Update()
    {
        inputDevice = InputManager.ActiveDevice;

        directionalInput = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));

        if (DialogueManager.IsConversationActive && recievingMovementInput == true)
        {
            recievingMovementInput = false;
        }
        else if(!DialogueManager.IsConversationActive && recievingMovementInput == false)
        {
            recievingMovementInput = true;
        }

        //If the player controller should be allowing the player to move right now
        if (recievingInput)
        {
            if (recievingMovementInput)
            {
                //Flips the players sprite and attack boxes
                if (directionalInput.x < -0.15f && !Player.instance.playerAnim.GetCurrentAnimatorStateInfo(0).IsName("Attacking") && !player.dashing) //If the players going left
                {
                    FlipTransformX(player.playerSprite.transform, true);
                    player.facingDirectionLeft = true;
                }
                else if (directionalInput.x > 0.15f && !Player.instance.playerAnim.GetCurrentAnimatorStateInfo(0).IsName("Attacking") && !player.dashing) //If the players going right
                {
                    FlipTransformX(player.playerSprite.transform, false);
                    player.facingDirectionLeft = false;
                }

                player.SetDirectionalInput(directionalInput); //Set up the directional input in the player script

                if (Input.GetKeyDown(KeyCode.Space) || inputDevice.Action1.WasPressed)
                {
                    player.OnJumpInputDown();
                }
                if (Input.GetKeyUp(KeyCode.Space) || inputDevice.Action1.WasReleased)
                {
                    player.OnJumpInputUp();
                }
                if (Input.GetMouseButtonDown(0) || inputDevice.Action3.WasPressed)
                {
                    player.GetComponent<PlayerAttack>().Attack();
                }
                if (Input.GetKeyDown(KeyCode.LeftShift) || inputDevice.RightBumper)
                {
                    player.OnDashInputDown();
                }
                
            }
            else if (!recievingMovementInput) //if it isn't able to take input right now. Nullify it's movement
            {
                player.SetDirectionalInput(Vector2.zero);
            }

            if ((Input.GetKey(KeyCode.E) || inputDevice.Action2.IsPressed) && Player.instance.boneArmorUnlocked) //Charge input
            {
                player.bam.Charge();
            }
            if ((Input.GetKeyUp(KeyCode.E) || inputDevice.Action2.WasReleased) && Player.instance.boneArmorUnlocked) //Stop chargin boi
            {
                player.bam.StopCharging();
            }

            if((Input.GetKey(KeyCode.Q) || inputDevice.Action4) && Player.instance.barkUnlocked) //Start charging bark
            {
                player.playerAttack.ChargeBark();
            }
            if((Input.GetKeyUp(KeyCode.Q) || inputDevice.Action4.WasReleased) && Player.instance.barkUnlocked) //Release bark
            {
                player.playerAttack.ReleaseBark();
                player.GetComponent<ProximitySelector>().UseCurrentSelection();
                if (DialogueManager.IsConversationActive)
                {
                    continueButton.onClick.Invoke();
                }
                
            }

            if(Input.GetKeyDown(KeyCode.Escape) || inputDevice.RightCommand.WasPressed) //If the player has hit a pause key
            {
                if (!settingsObject.activeSelf)
                {
                    settingsObject.SetActive(true);
                    EventSystem.current.SetSelectedGameObject(null);
                    EventSystem.current.SetSelectedGameObject(settingsObject.GetComponent<SettingsFunctionality>().resumeButton);
                    Player.instance.timeManager.PauseGame();
                    Player.instance.footsteps.Stop(Player.instance.PlayerAudioManager);
                    Player.instance.bam.boneArmorCharge.Stop(Player.instance.PlayerAudioManager);
                    openJournal.Post(Player.instance.PlayerAudioManager);
                }
            }
        }
        else if (!recievingInput || player.dashing) //if it isn't able to take input right now. Nullify it's movement
        {
            player.SetDirectionalInput(Vector2.zero);
            player.bam.StopCharging();
        }
        
    }

    public void FlipTransformX(Transform toFlip, bool negative)
    {
        if(negative && Mathf.Sign(toFlip.transform.localScale.x) == 1)
        {
            toFlip.localScale = new Vector2(-toFlip.localScale.x, toFlip.localScale.y);
        }
        else if (!negative && Mathf.Sign(toFlip.transform.localScale.x) == -1)
        {
            toFlip.localScale = new Vector2(Mathf.Abs(toFlip.localScale.x), toFlip.localScale.y); 
        }
    }

    public void OpenJournal()
    {
        openJournal.Post(Player.instance.PlayerAudioManager);
        recievingInput = false;
        player.footsteps.Stop(player.PlayerAudioManager);
    }
    public void CloseJournal()
    {
        closeJournal.Post(Player.instance.PlayerAudioManager);
        recievingInput = true;
    }
}
