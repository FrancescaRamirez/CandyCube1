using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{
    public Transform attackPoint;

    public float attackRangeHorizontal = 0.5f;
    public float attackRangeVertical = 0.5f;

    public float attackSize = 0.5f;

    public LayerMask hitLayer;
    public LayerMask obstacleLayer;

    public ParticleSystem playerAttackParticle;

    public int damage = 1;

    public AK.Wwise.Event swordSwing;
    public AK.Wwise.Event swordHitEnemy;
    public AK.Wwise.Event swordHitWall;
    public AK.Wwise.Event bark;
    public AK.Wwise.Event smallBark;

    bool barkCharge = true;
    bool currentlyBarkCharging = false;
    float barkCooldownTime = 2;
    float barkTimer = 2;
    float barkChargeAmount;
    public float barkChargeMaximum = 0.3f;
    float barkMaxSize = 20;
    public bool barkAttackVisual;

    public ParticleSystem BarkChargePart;
    public ParticleSystem BigBarkWavePart;
    public ParticleSystem SmallBarkWavePart;

    public float upDownAttackCutoff = 0.25f;

    private void Update()
    {
        if (barkTimer > 0 && (!Player.instance.playerAnim.GetCurrentAnimatorStateInfo(0).IsName("ChargingBark") || !Player.instance.playerAnim.GetCurrentAnimatorStateInfo(0).IsName("Bark")))
        {
            barkTimer -= Time.deltaTime;
        }
    }

    public void Attack()
    {
        if (!Player.instance.playerAnim.GetCurrentAnimatorStateInfo(0).IsName("Attacking")) //If currently attacking don't attack
        {
            Collider2D[] hitEnemies; //Initialize hitenemies array
            var playerAttackParticleMain = playerAttackParticle.main; //Get a refrence to the main of the player attack particle
            playerAttackParticleMain.startRotation3D = true;

            swordSwing.Post(Player.instance.PlayerAudioManager); //play sword Swing sound

            Vector3 currentDirectionalInput = Player.instance.playerInput.directionalInput; //Grab the current directional input to check if they are aiming up or down
            
            
            if(currentDirectionalInput.y > upDownAttackCutoff || currentDirectionalInput.y < -upDownAttackCutoff) //vertical attack
            {
                bool up = currentDirectionalInput.y > upDownAttackCutoff;
                
                Player.instance.playerAnim.SetTrigger(up ? "AttackUp" : "AttackDown");
                //Player.instance.playerAnim.SetTrigger("Attack"); //Placeholder attack anim
                
                hitEnemies = Physics2D.OverlapCircleAll(new Vector2(attackPoint.position.x, 
                    up ? (attackPoint.position.y + attackRangeVertical) : (attackPoint.position.y - attackRangeVertical)), attackSize, hitLayer);

                //Set particle roation accordingly
                playerAttackParticle.transform.rotation = Quaternion.Euler(0, 0, Player.instance.facingDirectionLeft ? (up ? 270 : 90) : (up ? 90 : 270));
                playerAttackParticleMain.startRotationZ = up ? 90 : -90;
            }
            else //horizontal attack
            {
                Player.instance.playerAnim.SetTrigger("Attack");
                hitEnemies = Physics2D.OverlapCircleAll(new Vector2(Player.instance.facingDirectionLeft ?
                    (attackPoint.position.x - attackRangeHorizontal) : (attackPoint.position.x + attackRangeHorizontal), attackPoint.position.y), attackSize, hitLayer);

                //Set particle roation accordingly
                playerAttackParticle.transform.rotation = Quaternion.Euler(0, 0, 0);
                playerAttackParticleMain.startRotationZ = 0;
            }

            playerAttackParticle.gameObject.transform.localScale = new Vector2(Player.instance.facingDirectionLeft ? -1 : 1, 1); //Flip the attack particle in the correct direction
            playerAttackParticle.Emit(1); //Emit a particle

            //Damage enemies that have been hit
            foreach (Collider2D hit in hitEnemies)
            {
                if (hit.tag == "Enemy")
                {
                    swordHitEnemy.Post(Player.instance.PlayerAudioManager);

                    Debug.Log("we hit" + hit.name);
                    hit.gameObject.GetComponent<Health>().TakeDamage(damage);

                    var dir = Player.instance.transform.position.x < hit.gameObject.transform.position.x ? 1 : -1;
                    if(hit.GetComponent<EnemyMain>() != null)
                    {
                        float stagger = hit.GetComponent<EnemyMain>().staggerForce;

                        if (stagger > 0)
                        {
                            if (dir == 1) //If the enemy is on the right
                            {
                                hit.gameObject.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                                hit.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(1, 0.5f) * stagger, ForceMode2D.Impulse); //Push it to the right
                            }
                            else if (dir == -1) //If the enemy is on the left 
                            {
                                hit.gameObject.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                                hit.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(-1, 0.5f) * stagger, ForceMode2D.Impulse); //Push it to the left
                            }
                        }
                    }

                }

                if(hit.tag == "Interactable")
                {
                    if (hit.GetComponent<BreakableObject>())
                    {
                        hit.GetComponent<BreakableObject>().Break();
                    }
                }


            }
        }
    }

    public void ChargeBark()
    {
        if ((barkChargeAmount > barkChargeMaximum || !barkCharge)) //If the bark has charged the maximum amount or it is not in charge mode set it to max and release it
        {
            barkTimer = barkCooldownTime; //Reset bark timer
            barkChargeAmount = barkChargeMaximum; //Set charge amount to max
            
            ReleaseBark();
        }
        
        if (barkTimer < 0 && !currentlyBarkCharging)
        {
            Player.instance.playerAnim.SetTrigger("BarkCharge"); //Start bark charge animation
            currentlyBarkCharging = true;
            Player.instance.playerInput.recievingMovementInput = false;
        }
        else if (currentlyBarkCharging)
        {
            barkChargeAmount += Time.deltaTime; //Increment bark charge amount
            print(barkChargeAmount);

            BarkChargePart.Play(true);
        }
    }

    public void ReleaseBark()
    {
        BarkChargePart.Stop();

        if (barkChargeAmount == barkChargeMaximum) //If they charged then do the attack
        {
            barkTimer = barkCooldownTime; //Start timer from top
            currentlyBarkCharging = false; //Not charging anymore

            Player.instance.playerAnim.SetTrigger("Bark"); //Play release bark anim

            //The size currently will always be barkMaxSize (Toggle bark attack visual bool in inspector to see the radius)

            Collider2D[] hitEnemies; //Initialize hitenemies array

            //Execute the bark stun attack radius depending on the charge amount
            hitEnemies = Physics2D.OverlapCircleAll(this.transform.position, (barkChargeAmount / barkChargeMaximum) * barkMaxSize, hitLayer);


            //Particle
            BigBarkWavePart.Play();

            bark.Post(Player.instance.PlayerAudioManager);

            //For each enemy contacted execute their stun function by an amount of the bark
            foreach (Collider2D enemy in hitEnemies)
            {
                var dir = Player.instance.transform.position.x < enemy.gameObject.transform.position.x ? 1 : -1;

                print("I just barked at" + enemy.name);

                if (enemy.attachedRigidbody)
                {
                    enemy.attachedRigidbody.velocity = Vector3.zero;
                }
                
                if (enemy.GetComponent<EnemyMain>())
                {
                    enemy.GetComponent<EnemyMain>().Stagger(); //Access the stagger function and play it
                    float stagger = enemy.GetComponent<EnemyMain>().staggerForce;

                    if (stagger > 0)
                    {
                        if (dir == 1) //If the enemy is on the right
                        {
                            enemy.gameObject.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                            enemy.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(1, 0.5f) * stagger, ForceMode2D.Impulse); //Push it to the right
                        }
                        else if (dir == -1) //If the enemy is on the left 
                        {
                            enemy.gameObject.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                            enemy.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(-1, 0.5f) * stagger, ForceMode2D.Impulse); //Push it to the left
                        }
                    }
                }
            }

            print("I Just barked this much: " + barkChargeAmount);

            barkChargeAmount = 0; //Reset bark charge amount

            Player.instance.playerInput.recievingMovementInput = true; //Re-enable movement
        }
        else if (barkChargeAmount > 0)
        {
            //Play the small bark particle
            //Play the small bark anim

            smallBark.Post(Player.instance.PlayerAudioManager);

            SmallBarkWavePart.Play();
            barkChargeAmount = 0; //Reset bark charge amount

            Player.instance.playerInput.recievingMovementInput = true; //Re-enable movement
        }
        else
        {
            Player.instance.playerInput.recievingMovementInput = true; //Re-enable movement
        }
        
    }

    private void OnDrawGizmosSelected()
    {
        if(attackPoint == null)
        {
            return;
        }

        Gizmos.DrawWireSphere(new Vector2(attackPoint.position.x, attackPoint.position.y + attackRangeVertical), attackSize); //Draw horizontal attack

        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(new Vector2(attackPoint.position.x + attackRangeHorizontal, attackPoint.position.y), attackSize); //Draw vertical attack

        if(barkAttackVisual == true)
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawWireSphere(this.transform.position, barkMaxSize);
        }
        
    }


}
