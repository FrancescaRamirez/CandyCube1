using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractiveMusic : MonoBehaviour
{
    public AK.Wwise.State combat;
    public AK.Wwise.State exploration;

    public AK.Wwise.Event startMusic;
    public AK.Wwise.Event stopMusic;

    public float combatDistance = 2;
    bool inCombat;

    GameObject[] enemies;

    public float boxXSize = 20;
    public float boxYSize = 10;

    public int enemyLayer = 7;

    public GameObject thisPlayer;

    public float bufferTime = 3;
    float bufferTimer = 0;

    bool musicStarted = false;

    private void Start()
    {
        thisPlayer = Player.instance.gameObject;
    }

    private void Update()
    {
        if (!musicStarted)
        {
            startMusic.Post(this.gameObject);
            musicStarted = true;
            exploration.SetValue();
        }


        //if (Physics2D.OverlapBox(Player.instance.transform.position, new Vector2(boxXSize, boxYSize), 0, 1 << enemyLayer))
        //{
        //    inCombat = true;
        //}
        //else if (bufferTimer > bufferTime)
        //{
        //    inCombat = false;
        //    bufferTimer = 0;
        //}
        //else
        //{
        //    bufferTimer += Time.deltaTime;
        //}

        //if (inCombat)
        //{
        //    combat.SetValue();
        //}
        //else
        //{
        //    exploration.SetValue();
        //}
    }


    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireCube(thisPlayer.transform.position, new Vector3(boxXSize, boxYSize, 1));
    }

    public void StopMusic()
    {
        stopMusic.Post(gameObject);
    }

    public void CombatSwitch()
    {
        combat.SetValue();
    }

    public void ExplorationSwitch()
    {
        exploration.SetValue();
    }
}
