using AnyPortrait;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using PixelCrushers.DialogueSystem;
using UnityEngine.SceneManagement;

public class PlayerHealth : MonoBehaviour
{
    int maximumHealth;
    public int currentHealth;
    public float hitCooldownTimer = 0;
    public float hitCooldownMaximum;

    public SpriteRenderer sr;
    Color hitColor;
    Color startColor;

    public apPortrait ap;

    public GameObject deathParticle;

    public GameObject bone;

    public float maxBoneVelocity = 16;
    public float minBoneVelocity = 12;

    public List<GameObject> respawnPoints;
    GameObject closestRespawnPoint;
    public GameObject respawnParticles;

    //public Slider healthBar;

    public List<GameObject> healthBarComponents;

    BoneArmorManager bam;
    public float hitFreezeDuration;
    public float hitFreezeScale;

    public AK.Wwise.Event death;

    private void Start()
    {
        maximumHealth = currentHealth;

        //healthBar.maxValue = maximumHealth;
        //healthBar.value = maximumHealth;

        foreach (GameObject bar in healthBarComponents)
        {
            bar.SetActive(true);
        }

        bool reaspawn = DialogueLua.GetVariable("IsRespawning").AsBool;
        

        if (gameObject.GetComponent<SpriteRenderer>()) //Look for a sprite
        {
            sr = gameObject.GetComponent<SpriteRenderer>();
        }
        else if (gameObject.GetComponentInChildren<SpriteRenderer>())
        {
            sr = gameObject.GetComponentInChildren<SpriteRenderer>();
        }

        if (sr != null)
        {
            startColor = sr.color;
            hitColor = new Color(sr.color.r - 50, sr.color.g - 50, sr.color.b - 50);
        }

    }

    private void Update()
    {
        if (hitCooldownTimer > 0)
        {
            hitCooldownTimer -= Time.deltaTime;
        }
        else if (sr == null) { }
        else if (sr.color != startColor)
        {
            sr.color = startColor;
        }
    }

    public void TakeDamage(int damage)
    {
        if(Player.instance.dead == false) //If it's already dead no reason to take more damage
        {
            if (hitCooldownTimer <= 0)
            {

                bool armorAbsorbedHit = Player.instance.bam.DamageArmor();

                if (!armorAbsorbedHit)
                {
                    currentHealth -= damage;

                    RefreshHealthVisuals();
                }

                hitCooldownTimer = hitCooldownMaximum;

                if (currentHealth > 0)
                    Player.instance.timeManager.freezeEvent(hitFreezeDuration, hitFreezeScale); //Freeze frame
            }

            if (currentHealth <= 0)
            {

                if (DestructionDodger.instance.respawnPosition == null)
                {
                    DestructionDodger.instance.currentlyTransfering = true;
                    currentHealth = maximumHealth;
                }

                Player.instance.dead = true;
                StartCoroutine("Respawn"); //Begins the respawn process
            }
        } 

    }

    public void RegainHealth()
    {
        if (currentHealth < maximumHealth)
        {
            Debug.Log("entered");
            currentHealth++;

            RefreshHealthVisuals();
        }
    }

    public void RefreshHealthVisuals()
    {
        healthBarComponents[0].SetActive(currentHealth >= 1 ? true : false);
        healthBarComponents[1].SetActive(currentHealth >= 2 ? true : false);
        healthBarComponents[2].SetActive(currentHealth >= 3 ? true : false);
    }

    public IEnumerator Respawn()
    {
        //Do fall down anim
        Player.instance.playerInput.recievingInput = false;
        Player.instance.playerAnim.SetTrigger("Faint");

        death.Post(Player.instance.PlayerAudioManager);

        yield return new WaitForSeconds(1f);

        //Begin teleport particle
        Instantiate(respawnParticles, Player.instance.transform.position, Quaternion.identity);
        yield return new WaitForSeconds(0.2f);

        //Dissapear player
        Player.instance.playerAnim.gameObject.SetActive(false);

        yield return new WaitForSeconds(1f);

        Player.instance.teleportScreen.SetActive(true);
        Player.instance.timeManager.PauseGame();

        Player.instance.playerAnim.gameObject.SetActive(true);

        Vector3 tempPosition = DestructionDodger.instance.respawnPosition.transform.position;
        transform.position = tempPosition;
        
        currentHealth = maximumHealth;
        foreach (GameObject bar in healthBarComponents)
        {
            bar.SetActive(true);
        }

        Player.instance.bam.boneCount = 0;

        DialogueLua.SetVariable("IsRespawning", true);

        Player.instance.loadingManager.StartCoroutine("LoadYourAsyncScene", SceneManager.GetActiveScene().name);
    }
}
