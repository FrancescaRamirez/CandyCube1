using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ZoneTransfer : MonoBehaviour
{
    public string sceneHeadedTo;

    public int transferNumber = 1;

    DestructionDodger dd;

    public GameObject spawnPoint;

    private void Start()
    {
        dd = DestructionDodger.instance;

        if (dd.currentlyTransfering && dd.transferNumber == transferNumber)
        {
            Player.instance.transform.position = spawnPoint.transform.position;

            dd.currentlyTransfering = false;
            if(dd.currentRestPoint != null)
            {
                dd.currentRestPoint.DeactivateRestSpot();
            }
            dd.currentRestPoint = null;
            dd.respawnPosition = spawnPoint;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            dd.transferNumber = transferNumber;
            dd.currentlyTransfering = true;

            Player.instance.footsteps.Stop(Player.instance.PlayerAudioManager);
            Player.instance.bam.boneArmorCharge.Stop(Player.instance.PlayerAudioManager);

            Player.instance.teleportScreen.SetActive(true);

            Player.instance.timeManager.PauseGame();

            Player.instance.loadingManager.StartCoroutine("LoadYourAsyncScene", sceneHeadedTo);
        }
    }

}
