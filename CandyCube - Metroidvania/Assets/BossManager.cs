using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BossManager : MonoBehaviour
{

    public GameObject healthBar;

    Slider bossHealthBar;

    public GameObject enemyHolder;

    Health thisHealth;

    public GameObject VictoryLore;

    private void Start()
    {
        bossHealthBar = healthBar.GetComponent<Slider>();
        thisHealth = this.GetComponent<Health>();

        SetMaxBossHealth(thisHealth.currentHealth);
    }

    private void Update()
    {
        if(bossHealthBar.value != thisHealth.currentHealth)
        {
            bossHealthBar.value = thisHealth.currentHealth;
        }
    }

    public void SetMaxBossHealth(int health)
    {
        bossHealthBar.maxValue = health;
        bossHealthBar.value = health;
    }

    public void BossDeath()
    {
        if(enemyHolder.transform.childCount > 0)
        {
            foreach (Transform child in enemyHolder.transform)
            {
                Destroy(child.gameObject);
            }
        }

        healthBar.SetActive(false);


        //Spawn lore to have final convo with
        VictoryLore.transform.position = new Vector2(transform.position.x, VictoryLore.transform.position.y);
    }

}
