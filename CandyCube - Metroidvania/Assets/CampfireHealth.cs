using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CampfireHealth : MonoBehaviour
{

    public int WaitTimerMax;

    public float WaitTimer;

    PlayerHealth Ph;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }


    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {


            if (WaitTimer >= WaitTimerMax)
            {
                // Debug.Log("entered");

                Ph = collision.gameObject.GetComponent<PlayerHealth>();
                WaitTimer = 0;

                //  Debug.Log("got");

                Ph.RegainHealth();
                //   Debug.Log("triggered");
            }
            else
            {
                WaitTimer += Time.deltaTime;
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        WaitTimer = 0;
    }

}