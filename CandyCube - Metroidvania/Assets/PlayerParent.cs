using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerParent : MonoBehaviour
{
    private void Start()
    {
        if (GetComponentInChildren<Player>().gameObject == Player.instance.gameObject)
        {
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    public void DieMyself()
    {
        Destroy(this.gameObject);
    }
}
