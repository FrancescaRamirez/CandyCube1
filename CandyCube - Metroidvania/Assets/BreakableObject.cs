using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreakableObject : MonoBehaviour
{

    public GameObject partPrefab;

    public float spawnAreaX = 2;
    public float spawnAreaY = 2;

    public float velocityMaxX = 16;
    float velocityMaxXStart; 
    public float velocityMaxY = 14;
    public float velocityMinY = 3;

    public int numberOfParts;

    public GameObject thingToDestroy;

    public AK.Wwise.Event rockBreak;

    private void Start()
    {
        velocityMaxXStart = velocityMaxX;
    }

    public void Break()
    {
        GameObject placeholder; 

        if(Player.instance.transform.position.x < transform.position.x) //If the players on the left of the object
        {
            velocityMaxX = velocityMaxXStart;
        }
        else
        {
            velocityMaxX = -velocityMaxXStart;
        }

        for (int i = 0; i < numberOfParts; i++)
        {
            placeholder = Instantiate(partPrefab, new Vector3(transform.position.x + Random.Range(-spawnAreaX, spawnAreaX), transform.position.y + Random.Range(-spawnAreaY, spawnAreaY), 0), Quaternion.Euler(0, 0, Random.Range(180, -180)));
            placeholder.GetComponent<Rigidbody2D>().velocity = new Vector3(Random.Range(0, velocityMaxX), Random.Range(velocityMinY, velocityMaxY), 0);
        }

        rockBreak.Post(Player.instance.PlayerAudioManager);

        Destroy(thingToDestroy);
    }

}
