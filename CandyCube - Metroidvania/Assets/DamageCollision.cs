using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageCollision : MonoBehaviour
{

    public AK.Wwise.Event thisCollisionNoise;

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            Player.instance.playerHealth.TakeDamage(1);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player" && thisCollisionNoise != null)
        {
            thisCollisionNoise.Post(Player.instance.PlayerAudioManager);
        }
    }

}
