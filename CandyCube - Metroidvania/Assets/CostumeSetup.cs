using AnyPortrait;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CostumeSetup : MonoBehaviour
{
    public apPortrait costume1;
    public apPortrait costume2;

    public Transform characterGroup;
    public apPortrait mainCharacter;

    void Start()
    {
        //Register as a child of Character Group and initialize Local location.
        //costume1.transform.parent = characterGroup;
        costume1.transform.localPosition = Vector3.zero;

        //Synchronize bone movements.
        costume1.Synchronize(mainCharacter, false, false, false, true, SYNC_BONE_OPTION.MatchFromRoot);

        //Modify the sorting order of the Costume 1 meshes based on the meshes of the Main Character.
        //Scarf
        costume1.SetSortingOrder("ScarfBody", mainCharacter.GetSortingOrder("Head") + 1);
        costume1.SetSortingOrder("RTie", mainCharacter.GetSortingOrder("Torso") - 1);
        costume1.SetSortingOrder("LTie", costume1.GetSortingOrder("RTie") - 1);
        //Kneepads
        costume1.SetSortingOrder("RKneePad", mainCharacter.GetSortingOrder("RFoot") + 1);
        costume1.SetSortingOrder("RLegStrapDown", mainCharacter.GetSortingOrder("RCalf") + 1);
        costume1.SetSortingOrder("RLegStrapUp", mainCharacter.GetSortingOrder("RCalf") + 1);
        costume1.SetSortingOrder("LKneePad", mainCharacter.GetSortingOrder("LFoot") + 1);
        costume1.SetSortingOrder("LLegStrapDown", mainCharacter.GetSortingOrder("LCalf") + 1);
        costume1.SetSortingOrder("LLegStrapUp", mainCharacter.GetSortingOrder("LCalf") + 1);



        //Register as a child of Character Group and initialize Local location.
        //costume2.transform.parent = characterGroup;
        costume2.transform.localPosition = Vector3.zero;

        //Synchronize bone movements.
        costume2.Synchronize(mainCharacter, false, false, false, true, SYNC_BONE_OPTION.MatchFromRoot);

        //Modify the sorting order of the Costume 1 meshes based on the meshes of the Main Character.
        //Scarf
        costume2.SetSortingOrder("BoneHelmet", mainCharacter.GetSortingOrder("Nose") + 1);
        costume2.SetSortingOrder("BoneNeckRing", mainCharacter.GetSortingOrder("LEar") + 1);
        costume2.SetSortingOrder("BoneChestPlate", mainCharacter.GetSortingOrder("Torso") + 1);

        //ArmBraces
        costume2.SetSortingOrder("RBoneArmBrace", mainCharacter.GetSortingOrder("RArmUp") + 1);
        costume2.SetSortingOrder("RBoneHandGuard", mainCharacter.GetSortingOrder("RPaw") + 1);
        costume2.SetSortingOrder("LBoneArmBrace", mainCharacter.GetSortingOrder("LArmUp") + 1);
        costume2.SetSortingOrder("LBoneHandGuard", mainCharacter.GetSortingOrder("LPaw") + 1);

        //Kneepads
        costume2.SetSortingOrder("RBoneKneePad", mainCharacter.GetSortingOrder("RFoot") + 1);
        costume2.SetSortingOrder("RBoneKneeStrapDown", mainCharacter.GetSortingOrder("RCalf") + 1);
        costume2.SetSortingOrder("RBoneKneeStrapUp", mainCharacter.GetSortingOrder("RCalf") + 1);
        costume2.SetSortingOrder("LBoneKneePad", mainCharacter.GetSortingOrder("LFoot") + 1);
        costume2.SetSortingOrder("LBoneKneeStrapDown", mainCharacter.GetSortingOrder("LCalf") + 1);
        costume2.SetSortingOrder("LBoneKneeStrapUp", mainCharacter.GetSortingOrder("LCalf") + 1);
    }
}
