using Pathfinding;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyingAggroRange : MonoBehaviour
{
    AIDestinationSetter destinationSetter;
    public float aggroRange;

    private void Start()
    {
        destinationSetter = GetComponent<AIDestinationSetter>();
    }

    private void Update()
    {
        if (Vector3.Distance(transform.position, Player.instance.transform.position) < aggroRange)
        {
            destinationSetter.target = Player.instance.transform;
        }
        else
        {
            destinationSetter.target = null;
        }


    }


}
