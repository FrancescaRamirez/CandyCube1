using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class MainMenu_Script : MonoBehaviour
{
    public AK.Wwise.State explorationState;

    public GameObject credits;
    public GameObject main;

    public GameObject menuFirstButton;
    public GameObject backButton;

    private void Start()
    {
        if (VictoryLore.hasCompletedGame)
        {
            main.SetActive(false);
            credits.SetActive(true);
        }
    }

    public void PlayGame ()
    {
        if(explorationState != null)
        {
            explorationState.SetValue();
        }
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void QuitGame ()
    {
        Debug.Log("Quit!");
        Application.Quit();
    }

    public void ActivateSideMenus()
    {
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(backButton);
    }

    public void BackToMainMenu()
    {
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(menuFirstButton);
    }

}
