using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMain : MonoBehaviour
{
    public float staggerForce = 10;

    public bool staggered = false;
    bool stillStaggering = false;

    [SerializeField]
    Animator thisEnemyAnimator;

    public Behaviour behaviorToDisable;

    public float staggerDuration = 1;
    float staggerTimer = 0;

    public bool hasAggroRange = false;
    public float aggroRange = 1;

    private void Update()
    {
        if (staggered && !stillStaggering) //If it need to stagger but hasn't begun staggering yet
        {
            behaviorToDisable.enabled = false; //Disable movement
            //thisEnemyAnimator.SetTrigger("Staggered"); //Play stagger anim
            stillStaggering = true;
            staggerTimer = 0;
        }
        else if(staggered && stillStaggering && staggerTimer < staggerDuration)
        {
            staggerTimer += Time.deltaTime;
        }
        else if (staggerTimer > staggerDuration) //Check if the stagger anims done
        {
            behaviorToDisable.enabled = true; //If so re-enable normal behavior
            staggered = false;
            stillStaggering = false;
        }        

    }

    public void Stagger()
    {
        staggered = true;
    }

}
