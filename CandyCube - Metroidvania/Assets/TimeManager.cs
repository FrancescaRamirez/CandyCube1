using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeManager : MonoBehaviour
{

    public float freezeDurationDefault = 0.2f;
    public float freezeTimescaleDefault = 0.2f;

    public void freezeEvent(float freezeDuration, float freezeTimeScale)
    {
        if(freezeDuration == 0)
        {
            freezeDuration = freezeDurationDefault;
        }
        if (freezeTimeScale == 0)
        {
            freezeTimeScale = freezeTimescaleDefault;
        }

        StartCoroutine(FreezeFrame(freezeDuration, freezeTimeScale));
    }

    public IEnumerator FreezeFrame(float freezeDuration, float freezeTimeScale)
    {
        Time.timeScale = freezeTimeScale; //stop time dependent things

        yield return new WaitForSecondsRealtime(freezeDuration / 2); //a given amount of time later

        float timeElapsed = 0;

        while (timeElapsed < freezeDuration / 2)
        {
            //valueToLerp = Mathf.Lerp(startValue, endValue, timeElapsed / lerpDuration)
            Time.timeScale = Mathf.Lerp(freezeTimeScale, 1, timeElapsed / (freezeDuration / 2));

            timeElapsed += Time.unscaledDeltaTime;

            yield return null;
        }

        //resume time
        Time.timeScale = 1;
    }

    internal void freezeEvent(object deathFreezeDuration, object deathFreezeScale)
    {
        throw new NotImplementedException();
    }


    public void PauseGame()
    {
        Player.instance.playerInput.recievingInput = false;
        Time.timeScale = 0;
    }

    public void ResumeGame()
    {
        Player.instance.playerInput.recievingInput = true;
        Time.timeScale = 1;
    }
}
