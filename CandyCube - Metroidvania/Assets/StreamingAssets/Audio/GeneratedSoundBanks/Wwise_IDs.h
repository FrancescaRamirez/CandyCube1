/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID ABILITYPICKUP = 1638902313U;
        static const AkUniqueID ATTACK = 180661997U;
        static const AkUniqueID BARK = 1274655755U;
        static const AkUniqueID BARKSMALL = 1933917496U;
        static const AkUniqueID BARRENMUSIC = 2496685244U;
        static const AkUniqueID BONEBREAK = 1834806824U;
        static const AkUniqueID BONECHARGE = 4052738283U;
        static const AkUniqueID BONEFORM = 829341565U;
        static const AkUniqueID BONEPICKUP = 179264519U;
        static const AkUniqueID BOSSDEATH = 1481017468U;
        static const AkUniqueID BOSSLAUGH = 3485698645U;
        static const AkUniqueID BOSSQUEUE = 4175580539U;
        static const AkUniqueID BOSSSTEPS = 3347972775U;
        static const AkUniqueID BREAKROCK = 3614189553U;
        static const AkUniqueID CAMPFIREAMBIENCE = 280013598U;
        static const AkUniqueID DOUBLEJUMP = 1308315758U;
        static const AkUniqueID FIREDASH = 1576773933U;
        static const AkUniqueID FIREFROGEXPLOSION = 2971177040U;
        static const AkUniqueID FIREFROGSHOOT = 4096191584U;
        static const AkUniqueID FLAMINGPROJECTILELOOP = 2961035784U;
        static const AkUniqueID HITENEMY = 705923028U;
        static const AkUniqueID HITWALL = 4097863548U;
        static const AkUniqueID JOURNALCLOSE = 3987853926U;
        static const AkUniqueID JOURNALOPEN = 1738215602U;
        static const AkUniqueID JOURNALPICKUP = 1970538966U;
        static const AkUniqueID JUMP = 3833651337U;
        static const AkUniqueID LANDING = 2548270042U;
        static const AkUniqueID LAVAAMBIENCE = 3943171411U;
        static const AkUniqueID LAVAHIT = 3618953602U;
        static const AkUniqueID LIGHTFIRE = 690992255U;
        static const AkUniqueID LORE_SPEAK = 1899343210U;
        static const AkUniqueID LORETELEPORT = 3834716424U;
        static const AkUniqueID PLAY_FOOTSTEPS_STONE = 1339603617U;
        static const AkUniqueID PLAY_FOOTSTEPS_WOOD = 2064026589U;
        static const AkUniqueID PLAYERDAMAGE = 337406793U;
        static const AkUniqueID PLAYERDEATH = 1656947812U;
        static const AkUniqueID PLAYERTELEPORTIN = 2122750230U;
        static const AkUniqueID PLAYERTELEPORTOUT = 1056047161U;
        static const AkUniqueID STOPMUSIC = 1917263390U;
        static const AkUniqueID UIBUTTON = 1812936123U;
        static const AkUniqueID UIOPEN = 1389761091U;
        static const AkUniqueID WALLJUMP = 377821069U;
    } // namespace EVENTS

    namespace STATES
    {
        namespace LORE
        {
            static const AkUniqueID GROUP = 204455109U;

            namespace STATE
            {
                static const AkUniqueID NONE = 748895195U;
                static const AkUniqueID NOT_PLAYING = 3853799151U;
                static const AkUniqueID PLAYING = 1852808225U;
            } // namespace STATE
        } // namespace LORE

        namespace MAINSTATE
        {
            static const AkUniqueID GROUP = 1295275457U;

            namespace STATE
            {
                static const AkUniqueID COMBAT = 2764240573U;
                static const AkUniqueID EXPLORATION = 2582085496U;
                static const AkUniqueID NONE = 748895195U;
            } // namespace STATE
        } // namespace MAINSTATE

    } // namespace STATES

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID MAIN = 3161908922U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
    } // namespace BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
