using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PanleSize : MonoBehaviour
{
    public GameObject Panel;

    public Text PanleText;

    public Vector3 textPos;
    // Start is called before the first frame update
    void Start()
    {
        Panel.GetComponent<GameObject>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SizeUp()
    {
        Panel.transform.localScale = new Vector3(0.6f, 2.9f, 1);

        PanleText.transform.localScale = new Vector3(1 / 0.6f, 1 / 2.9f, 1);

        PanleText.rectTransform.offsetMax = new Vector2 (208, -141);
        PanleText.rectTransform.offsetMin = new Vector2(200, -159);


    }
}
