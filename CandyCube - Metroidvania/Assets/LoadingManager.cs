using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadingManager : MonoBehaviour
{

    public InteractiveMusic musicObject;

    public IEnumerator LoadYourAsyncScene(string sceneName)
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneName);

        musicObject.StopMusic();

        while (!asyncLoad.isDone)
        {
            yield return null;
        }

        Player.instance.teleportScreen.SetActive(false);
        Player.instance.timeManager.ResumeGame();

        if (Player.instance.dead)
        {
            Instantiate(Player.instance.playerHealth.respawnParticles, Player.instance.transform.position, Quaternion.identity);
            Player.instance.dead = false;
        }
    }
}
