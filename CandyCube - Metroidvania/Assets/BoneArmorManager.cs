using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BoneArmorManager : MonoBehaviour
{

    public List<GameObject> armorPieces;
    public List<GameObject> armorPieces2;
    public List<GameObject> armorPieces3;

    public int numberOfPieces = 3;

    public int currentBoneArmorCharge = 4;

    public int boneCount = 0;

    public int maxBoneCount = 15;

    [SerializeField]
    float chargeTimerMax = 1;
    float chargeTimer;

    public ParticleSystem boneFrags;
    public ParticleSystem BrokeBoneFrags;

    Text BoneScore;
    public GameObject BoneScoreConter;

    public Slider BoneHealthBar;
    public GameObject BoneHealthVisuals;

    public List<GameObject> boneArmorUISections;

    public AK.Wwise.Event boneArmorCharge;
    public AK.Wwise.Event boneArmorForm;
    public AK.Wwise.Event boneArmorBreak;

    bool charging = false;

    /// <summary>
    /// Function to charge bone armor
    /// </summary>
    /// <returns></returns>

    private void Start()
    {
        chargeTimer = chargeTimerMax;

        ArmorChanging(armorPieces, false);
        ArmorChanging(armorPieces2, false);
        ArmorChanging(armorPieces3, false);

        BoneScore = BoneScoreConter.GetComponent<Text>();
        BoneScore.text = "Bones: " + boneCount;

        foreach (GameObject bone in boneArmorUISections)
        {
            bone.SetActive(false);
        }

        BoneHealthVisuals.SetActive(false);
    }

    private void Update()
    {
        BoneScore.text = "Bones: " + boneCount + "/15";
    }

    public void Charge()
    {
        if(currentBoneArmorCharge < numberOfPieces && boneCount > 0) // Then you are allowed to charge
        {
            //Take away player movement input
            Player.instance.playerInput.recievingMovementInput = false;

            if (!charging)
            {
                boneFrags.Play();
                boneArmorCharge.Post(Player.instance.PlayerAudioManager);
                charging = true;
            }

            if (chargeTimer >= chargeTimerMax) // If charge is ready to be done and we haven't charged up to maximum
            {
                currentBoneArmorCharge += 1; //Charge up our lady
                chargeTimer = 0;
                boneCount--;

                boneArmorForm.Post(Player.instance.PlayerAudioManager);
                
                RefreshBoneArmorVisuals();
            }
            else //If charge hasn't finished yet
            {
                chargeTimer += Time.deltaTime;
            }
        }
        else
        {
            StopCharging();
        }

    }

    public void StopCharging()
    {
        Player.instance.playerInput.recievingMovementInput = true; //Give back player movement input

        boneArmorCharge.Stop(Player.instance.PlayerAudioManager);
        chargeTimer = 0;

        charging = false;

        //Stop any charging effects
        boneFrags.Stop();
        
    }


    public bool DamageArmor()
    {
        bool couldTakeDamage;

        if(currentBoneArmorCharge > 0)
        {
            currentBoneArmorCharge -= 1; //Subtract one from bone armor
            RefreshBoneArmorVisuals();
            couldTakeDamage = true;
            boneArmorBreak.Post(Player.instance.PlayerAudioManager);
        }
        else
        {
            couldTakeDamage = false;
        }

        return couldTakeDamage;
    }

    public void RefreshBoneArmorVisuals()
    {

        for (int i = 0; i < numberOfPieces; i++)
        {

            if(i < currentBoneArmorCharge) //If the current piece being adjusted is below currentCharge activate it
            {
                //armorPieces[i].SetActive(true);

                if(i == 0)
                {
                    boneArmorUISections[0].SetActive(true);
                    ArmorChanging(armorPieces, true);
                }
                else if(i == 1)
                {
                    boneArmorUISections[1].SetActive(true);
                    ArmorChanging(armorPieces2, true);
                }
                else if(i == 2)
                {
                    boneArmorUISections[2].SetActive(true);
                    ArmorChanging(armorPieces3, true);
                }

                BoneHealthVisuals.SetActive(true);
                BoneHealthBar.value = currentBoneArmorCharge;
            }
            else if(i >= currentBoneArmorCharge) //If the piece being adjusted is above current charge de-activate it
            {
                //armorPieces[i].SetActive(false);

                if (i == 0)
                {
                    boneArmorUISections[0].SetActive(false);
                    ArmorChanging(armorPieces, false);
                }
                else if (i == 1)
                {
                    boneArmorUISections[1].SetActive(false);
                    ArmorChanging(armorPieces2, false);
                }
                else if (i == 2)
                {
                    boneArmorUISections[2].SetActive(false);
                    ArmorChanging(armorPieces3, false);
                }

                BrokeBoneFrags.Play();
                BoneHealthBar.value = currentBoneArmorCharge;

                if (currentBoneArmorCharge == 0)
                {
                    BoneHealthVisuals.SetActive(false);
                }
            }
            
        }
    }

    public void ArmorChanging(List<GameObject> thisArmor, bool putOn)
    {
        foreach (GameObject armorPiece in thisArmor)
        {
            armorPiece.SetActive(putOn);
        }
    }
}
