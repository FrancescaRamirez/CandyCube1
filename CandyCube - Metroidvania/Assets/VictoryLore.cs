using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class VictoryLore : MonoBehaviour
{

    public static bool hasCompletedGame = false;

    InteractiveMusic musicObject;


    private void Start()
    {
        musicObject = Player.instance.interactiveMusic;
    }

    public void GoToMenu()
    {
        hasCompletedGame = true;

        StartCoroutine("sceneTransfer");
    }

    public void DestroyEverything()
    {
        Destroy(Player.instance.pp.gameObject); //Destroy player
        Player.instance = null; //Clear instance

        Destroy(DestructionDodger.instance.gameObject); //Destroy do not destroy parent
        DestructionDodger.instance = null; //Clear instance
    }

    IEnumerator sceneTransfer()
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(0);

        Player.instance.teleportScreen.SetActive(false);
        Player.instance.timeManager.PauseGame();

        DestroyEverything();

        musicObject.StopMusic();

        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }
}
