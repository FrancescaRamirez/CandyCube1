using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dissapear : MonoBehaviour
{
    SpriteRenderer sr;

    private void Start()
    {
        sr = GetComponent<SpriteRenderer>();
    }

    private void Update()
    {
        Color temp = sr.color;
        temp.a -= Time.deltaTime / 5;
        sr.color = temp; 

        if(sr.color.a <= 0)
        {
            Destroy(this.gameObject);
        }
    }

}
