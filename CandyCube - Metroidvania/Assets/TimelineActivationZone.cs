using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class TimelineActivationZone : MonoBehaviour
{
    public PlayableDirector timelineToPlay;

    public bool played = false;

    public float waitTime = 2;

    public AK.Wwise.State combat;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Player" && !played)
        {
            timelineToPlay.Play();
            Player.instance.playerInput.recievingInput = false;
            StartCoroutine("returnInput");
            played = true;
            if(combat != null)
            {
                combat.SetValue();
            }
        }
    }

    IEnumerator returnInput()
    {
        yield return new WaitForSeconds(waitTime);

        Player.instance.playerInput.recievingInput = true;
    }
}
