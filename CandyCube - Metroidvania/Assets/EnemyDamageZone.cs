using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDamageZone : MonoBehaviour
{
    Collider2D[] hits;

    Collider2D thisCollider;

    BoxCollider2D thisBoxCollider;
    CircleCollider2D thisCircleCollider;

    [SerializeField]
    Animator thisEnemyAnimator;

    private void Start()
    {
        thisEnemyAnimator = GetComponentInChildren<Animator>();
        thisCollider = GetComponent<Collider2D>();
        if(thisCollider is BoxCollider2D)
        {
            thisBoxCollider = (BoxCollider2D)thisCollider;
        }
        else if(thisCollider is CircleCollider2D)
        {
            thisCircleCollider = (CircleCollider2D)thisCollider;
        }
    }

    public void Update()
    {
        if(thisBoxCollider)
        {
            hits = Physics2D.OverlapBoxAll(transform.position, thisBoxCollider.size, 0, LayerMask.GetMask("Player"));
        }
        else if (thisCircleCollider)
        {
            hits = Physics2D.OverlapCircleAll(transform.position, thisCircleCollider.radius, LayerMask.GetMask("Player"), 0);
        }

        if (hits.Length > 0)
        {
            thisEnemyAnimator.SetTrigger("Attack");
            Player.instance.playerHealth.TakeDamage(1);
        }
    }

    
}
