using UnityEngine;

namespace BehaviorDesigner.Runtime.Tasks.Unity.UnityGameObject
{
    [TaskCategory("Unity/GameObject")]
    [TaskDescription("Instantiates a new GameObject. Returns Success.")]
    public class Instantiate : Action
    {
        [Tooltip("The GameObject that the task operates on. If null the task GameObject is used.")]
        public SharedGameObject targetGameObject;
        [Tooltip("The position of the new GameObject")]
        public SharedVector3 position;
        [Tooltip("The rotation of the new GameObject")]
        public SharedQuaternion rotation = Quaternion.identity;
        [SharedRequired]
        [Tooltip("The instantiated GameObject")]
        public SharedGameObject storeResult;

        [Tooltip("Is there a parent object or not")]
        public SharedBool parented;
        [Tooltip("The parent object (If there is one)")]
        public SharedGameObject parentObject;
        [Tooltip("Starting velocity of the object (If rigidbody2d)")]
        public SharedVector3 startingVelocity;

        public override TaskStatus OnUpdate()
        {
            if (parented.Value == false)
            {
                storeResult.Value = GameObject.Instantiate(targetGameObject.Value, position.Value, rotation.Value) as GameObject;
            }
            else if(parented.Value == true)
            {
                storeResult.Value = GameObject.Instantiate(targetGameObject.Value, position.Value, rotation.Value, parentObject.Value.transform) as GameObject;
            }

            if (startingVelocity.Value != Vector3.zero)
            {
                storeResult.Value.GetComponent<Rigidbody2D>().velocity = startingVelocity.Value;
            }

            return TaskStatus.Success;
        }

        public override void OnReset()
        {
            targetGameObject = null;
            position = Vector3.zero;
            rotation = Quaternion.identity;
        }
    }
}